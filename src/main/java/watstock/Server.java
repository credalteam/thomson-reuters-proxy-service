package watstock;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server
{
    class Session{
        Socket socket;
        Set tickers = null;

        private Session(Socket socket){
            this.socket = socket;
        }
    }
    private static Server singleton = null;
    private final int port;
    private boolean running;
    private final ExecutorService threadPool;
    private Thread serverThread;

    private int bufferSize;

    private List<Session> clients;

    private Server(int port)
    {
        this.port = port;

        clients = new ArrayList<>();
        threadPool = Executors.newFixedThreadPool(10,new ThreadFactory()
        {
            private final AtomicInteger instanceCount = new AtomicInteger();
            @Override
            public Thread newThread(Runnable r)
            {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                t.setName("HANDLER_" + instanceCount.getAndIncrement());
                return t;
            }
        });
    }

    public static void init(int port, int bufferSize){
        assert  (singleton == null);
        if (singleton != null)
            return;

        singleton = new Server(port);
        singleton.bufferSize = bufferSize;
        singleton.start();

        // JavaSparkSessionSingleton.getInstance(sparcCfg).sparkContext().broadcast(singleton);
    }

    public static Server getSingleton(){
        assert (singleton != null);
        return singleton;
    }

    private void start()
    {
        running = true;
        // System.out.println("------------- Starting Server Up -------------");
        serverThread = new Thread(() ->
        {
            try
            {
                ServerSocket server = new ServerSocket(port);
                server.setReuseAddress(true);

                while ( running)
                {
                    final Socket client = server.accept();
                    client.setKeepAlive(true);

                    Session session = new Session(client);


                    clients.add(session);

                }
            }
            catch (IOException ex)
            {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        serverThread.setName("LISTENER");
        serverThread.start();
    }


    public void send(String record){
        Set<Session> toBeRemoved = new HashSet();
        //ArrayList<Session> cl = clients.getValue();
        clients.forEach((client) -> {

                    try {
                        final PrintWriter out = new PrintWriter(client.socket.getOutputStream(), true);
                        out.println(record);
                        out.flush();
                    }catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        try {
                            toBeRemoved.add(client);
                            client.socket.close();
                        } catch (IOException ex2) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex2);
                        }
                    }

                }
        );
        clients.removeAll(toBeRemoved);
    }


    public void stop()
    {
        running = false;
        if ( serverThread != null)
        {
            serverThread.interrupt();
        }
        threadPool.shutdown();
        serverThread = null;

    }

    public static void main(String[] args)
    {
        Server server = new Server(2003);
        server.start();
    }

}