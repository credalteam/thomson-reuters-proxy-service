package watstock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.json.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ProxyConfig {
    private static ProxyConfig instance = null;

    public class TRConnection{
        public final String host;
        public final Integer port;
        public final String username;

        TRConnection(JsonNode json){
            host = json.get("host").asText();
            port = json.get("port").asInt();
            username     = json.get("username").asText();
        }

    }


    private List<TRConnection> connections;

    private Integer outputPort;
    private Integer buffSize;
    private String tickerFile;


    static void init(String cfgFile){
        assert (instance == null);
        instance = new ProxyConfig(cfgFile);
    }

    private ProxyConfig(String cfgFile){
        assert (instance == null);
        ObjectMapper mapper = new ObjectMapper();
        try {

            JsonNode cfg = mapper.readTree(new FileReader(cfgFile));

            connections = new ArrayList();

            cfg.get("tr_sources").elements().forEachRemaining(con -> connections.add(new TRConnection(con)));

            outputPort = cfg.get("out_port").asInt();
            buffSize = cfg.get("buffer_size").asInt();
            tickerFile = cfg.get("ticker_file").asText();

        }catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static Integer getBuffSize() {
        assert (instance != null);
        return instance.buffSize;
    }

    public static Integer getOutputPort() {
        assert (instance != null);
        return instance.outputPort;
    }

    public static List<TRConnection> getConnections() {
        assert (instance != null);
        return instance.connections;
    }

    public static String getTickerFile() {
        assert (instance != null);
        return instance.tickerFile;
    }
}
