package watstock;

import com.thomsonreuters.ema.access.*;

class Level1Client implements OmmConsumerClient
{
	public void onRefreshMsg(RefreshMsg refreshMsg, OmmConsumerEvent event)
	{
		Server.getSingleton().send(refreshMsg.toString());
	}

	public void onUpdateMsg(UpdateMsg updateMsg, OmmConsumerEvent event)
	{
		Server.getSingleton().send(updateMsg.toString());
	}


	public void onStatusMsg(StatusMsg statusMsg, OmmConsumerEvent event)
	{
		System.out.println(statusMsg);
	}

	public void onGenericMsg(GenericMsg genericMsg, OmmConsumerEvent consumerEvent){System.out.println(genericMsg);}
	public void onAckMsg(AckMsg ackMsg, OmmConsumerEvent consumerEvent){System.out.println(ackMsg);}
	public void onAllMsg(Msg msg, OmmConsumerEvent consumerEvent){}
}
