///*|----------------------------------------------------------------------------------------------------
// *|            This source code is provided under the Apache 2.0 license      	--
// *|  and is provided AS IS with no warranty or guarantee of fit for purpose.  --
// *|                See the project's LICENSE.md for details.                  					--
// *|           Copyright Thomson Reuters 2016. All rights reserved.            		--
///*|----------------------------------------------------------------------------------------------------

package watstock;

import com.thomsonreuters.ema.access.EmaFactory;
import com.thomsonreuters.ema.access.OmmConsumer;
import com.thomsonreuters.ema.access.OmmConsumerConfig;
import com.thomsonreuters.ema.access.ReqMsg;
import com.thomsonreuters.ema.rdm.EmaRdm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;


public class Consumer
{
	private static Set validTickers = null;
	private static boolean connected = false;

	private static void readTickerFile(String file){
		if (file == null || file.equalsIgnoreCase(""))
			return;
		Path filePath = Paths.get(file);

		try {
			validTickers = Files.readAllLines(filePath)
					.stream()
					.flatMap(line -> Arrays.stream(line.split( ",")))
					.map(String::trim)
					//.map(x ->x +".O")
					.collect(Collectors.toSet());
		}
		catch (IOException ex){
			java.util.logging.Logger.getLogger("Reading tickers").log(java.util.logging.Level.SEVERE, null, ex);
		}
	}

	private static void connect(){
		OmmConsumer consumer = null;

		while (connected) {

			try {

				List<ProxyConfig.TRConnection> availableConn = ProxyConfig.getConnections();

				int rnIdx = (int) (Math.random() * availableConn.size());

				ProxyConfig.TRConnection conn = availableConn.get(rnIdx);

				Level1Client level1Client = new Level1Client();
				Level2Client level2Client = new Level2Client();

				OmmConsumerConfig config = EmaFactory.createOmmConsumerConfig().consumerName("Consumer_1");



				consumer = EmaFactory.createOmmConsumer(
						config
								//.host(conn.host + ":" + conn.port)//"159.220.108.198:14002")
								.username(conn.username)//"NJ2_03_RHB_US86752")
				);

				ReqMsg reqMsg = EmaFactory.createReqMsg();

				//String ticker = args[3];//"AAPL.OQ";
				for (Object ticker : validTickers) {
					//consumer.registerClient(reqMsg.serviceName("hEDD").name(ticker.toString()), level1Client);

					consumer.registerClient(
							EmaFactory.createReqMsg()
									.domainType(EmaRdm.MMT_MARKET_BY_PRICE)
									.serviceName("hEDD")
									//.interestAfterRefresh(true)
									//.nameType(InstrumentNameTypes.RIC)
									.name(ticker.toString())
							, level2Client);

				}


				while (connected)
					Thread.sleep(1000 * 60);

			} catch (Exception ex) {
				java.util.logging.Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				if (consumer != null) consumer.uninitialize();
			}
		}
	}

	public static void main(String[] args)
	{
		if(args.length < 1)
			System.out.println("You need to provide config file");

		ProxyConfig.init(args[0]);
		readTickerFile(ProxyConfig.getTickerFile());
		connected = true;
		Server.init(ProxyConfig.getOutputPort(), ProxyConfig.getBuffSize());
		connect();
	}
}


