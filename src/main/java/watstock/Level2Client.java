package watstock;

import com.thomsonreuters.ema.access.*;

class Level2Client implements OmmConsumerClient
{
	public void onRefreshMsg(RefreshMsg refreshMsg, OmmConsumerEvent event)
	{
		System.out.println(refreshMsg);
	}

	public void onUpdateMsg(UpdateMsg updateMsg, OmmConsumerEvent event)
	{
		System.out.println(updateMsg);
	}

	public void onStatusMsg(StatusMsg statusMsg, OmmConsumerEvent event)
	{
		System.out.println(statusMsg);
	}

	public void onGenericMsg(GenericMsg genericMsg, OmmConsumerEvent consumerEvent){System.out.println(genericMsg);}
	public void onAckMsg(AckMsg ackMsg, OmmConsumerEvent consumerEvent){System.out.println(ackMsg);}
	public void onAllMsg(Msg msg, OmmConsumerEvent consumerEvent){System.out.println(msg);}
}
